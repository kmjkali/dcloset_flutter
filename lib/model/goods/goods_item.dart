

class GoodsItem {


  num id;
  // @변경 nullable 로 고침
  String productName;
  String productInfo;
  String productMainImage;
  bool ynMembership;
  bool ynPost;

  GoodsItem(this.id,this.productName,this.productInfo,this.productMainImage,this.ynMembership,this.ynPost);

  factory GoodsItem.fromJson(Map<String,dynamic>json){
    return GoodsItem(
        json['id'],
        json['productName'],
        json['productInfo'],
        json['productMainImage'],
        json['ynMembership'],
        json['ynPost']
    );
  }
}
