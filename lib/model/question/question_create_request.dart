class QuestionCreateRequest{

  // Q&A 질문하기

   String questionCreateDate;
   String questionTitle;
   int questionPassword;
   String questionContent;


   QuestionCreateRequest(
       this.questionCreateDate, this.questionPassword, this.questionTitle, this.questionContent);

   Map<String,dynamic>toJson() =>{
     'questionCreateDate':questionCreateDate,
     'questionTitle':questionTitle,
     'questionPassword':questionPassword,
     'questionContent':questionContent

   };

}