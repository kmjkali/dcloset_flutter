


import 'package:dclost_app/model/order/order_response.dart';

class OrderDetailResult {
  String msg;
  num code;
  OrderResponse data;

  OrderDetailResult(this.msg,this.code,this.data);

  factory OrderDetailResult.fromJson(Map<String,dynamic>json){
    return OrderDetailResult(
        json['msg'],
        json['code'],
        OrderResponse.fromJson(json['data']) // 맨위의 데이터로 바꿔줘
    );

  }
}