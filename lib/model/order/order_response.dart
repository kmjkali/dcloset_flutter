
class OrderResponse {
   num id;
   dynamic member;//to json 구현해서  제이슨안에 제이슨 넣어주자 (아님 오더에서 쓰는 멤버그릇을 만들어주던가)
   dynamic goods;
   String rentalType;
   String goodsSize;
   String goodsColor;
   String orderDate;
   String desiredDate;
   String deadlineDate;
   String orderStatus;

   OrderResponse(this.id,this.member,this.goods,this.rentalType,this.goodsSize,
       this.goodsColor,this.orderDate,this.desiredDate,this.deadlineDate,this.orderStatus);

   factory OrderResponse.fromJson(Map<String,dynamic>json){
     return OrderResponse(
         json['id'],
         json['member'],
         json['goods'],
         json['rentalType'],
         json['goodsSize'],
         json['goodsColor'],
         json['orderDate'],
         json['desiredDate'],
         json['deadlineDate'],
         json['orderStatus']);
   }
}