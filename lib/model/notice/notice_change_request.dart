class NoticeChangeRequest{


  DateTime noticeCreateDate;
  String noticeTitle;
  String noticeImage;
  String noticeContent;

  NoticeChangeRequest(
      this.noticeCreateDate,
      this.noticeTitle,
      this.noticeImage,
      this.noticeContent
      );

  factory NoticeChangeRequest.fromJson(Map<String,dynamic>json){
    return NoticeChangeRequest(
        json['noticeCreateDate'],
        json['noticeTitle'],
        json['noticeImage'],
        json['noticeContent']);
  }

  Map<String,dynamic>toJson() => {
    'noticeCreateDate':noticeCreateDate,
    'noticeTitle':noticeTitle,
    'noticeImage':noticeImage,
    'noticeContent':noticeContent




  };


}