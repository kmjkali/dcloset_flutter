class NoticeItem {

   num id;
   dynamic member;
   DateTime noticeCreateDate;
   String noticeTitle;
   String noticeContent;
   String noticeImage;

   NoticeItem(
       this.id,
       this.member,
       this.noticeCreateDate,
       this.noticeTitle,
       this.noticeContent,
       this.noticeImage);

   factory NoticeItem.fromJson(Map<String,dynamic>json){
     return NoticeItem(
         json['id'],
         json['member'],
         json['noticeCreateDate'],
         json['noticeTitle'],
         json['noticeContent'],
         json['noticeImage']
     );
   }

}