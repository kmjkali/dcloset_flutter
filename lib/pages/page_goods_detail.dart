import 'package:dclost_app/model/goods/goods_response.dart';
import 'package:dclost_app/repository/repo_goods.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PageGoodsDetail extends StatefulWidget {

  const PageGoodsDetail({super.key, required this.id});

  final num id;

  // 제품 상세보기 페이지
  // 하단 주문하기 버튼 만들어야 함. ( 바텀네비바? 고정 버튼? 드로어?.. )
  // 제품 상세 정보 내용 좀더 그럴싸하게 디자인해야 함.

  @override
  State<PageGoodsDetail> createState() => _PageGoodsDetailState();
}

class _PageGoodsDetailState extends State<PageGoodsDetail> {

  GoodsResponse? _detail;

  Future<void> _loadDetail() async {
    await RepoGoods().getGoods(widget.id)
        .then((res) => {
      setState(() {
        if ( res.code == 0 ){
          _detail = res.data;
        } else {
          Fluttertoast.showToast(msg: res.msg);
          // @TODO 예외처리
        }
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('상품 상세보기'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Column(
                children: [
                  Image.network('widget.GoodsResponse.productMainImage'),//
                  Text(
                     'widget.GoodsResponse.productName'
                  ),
                  Text(
                    'widget.GoodsResponse.productInfo'
                  )
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  Image.network('widget.GoodsResponse.productSubImage1'),
                  Image.network('widget.GoodsResponse.productSubImage2'),
                  Image.network('widget.GoodsResponse.productSubImage3')
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
