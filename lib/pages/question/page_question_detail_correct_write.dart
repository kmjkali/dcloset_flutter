import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:flutter/material.dart';

class PageQuestionDetailCorrectWrite extends StatefulWidget {
  const PageQuestionDetailCorrectWrite({super.key});

  @override
  State<PageQuestionDetailCorrectWrite> createState() => _PageQuestionDetailCorrectWriteState();
}

class _PageQuestionDetailCorrectWriteState extends State<PageQuestionDetailCorrectWrite> {

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              width: 400,
              margin: EdgeInsets.all(10),
              child: TextField(
                decoration: const InputDecoration(
                  labelText: '제목',
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                        width: 10.0,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              width: 400,
              margin: EdgeInsets.all(10),
              child: TextField(
                maxLength: 1000,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.white,
                      width: 5.0,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(0)),
                  ),
                ),
                maxLines: 11,
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: ElevatedButton(
                onPressed: (){
                  showDialog(context: context, builder: ((context) => AlertDialog(
                    title: Text(
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 22,

                        ),'Q&A 게시판'
                    ),
                    content: Text('수정 완료 되었습니다'),
                    actions: [
                      TextButton(
                        onPressed: () => Navigator.pop(context, 'ok'), child: Text(
                          style: TextStyle(
                            color: Colors.black87,
                            fontWeight: FontWeight.bold,
                          ),
                          'ok'),
                      ),
                      TextButton(onPressed: () => Navigator.pop(context, 'Cancel'), child: Text(
                          style: TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                          'cancel'),
                      ),
                    ],
                  )));
                },
                style: ElevatedButton.styleFrom(
                  //엘리베이터 버튼 스타일
                  minimumSize: Size(70 ,40), // 버튼 사이즈 조절
                  backgroundColor: Colors.black,
                  foregroundColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                ),
                child: Text('수정하기'),
              ),
            ),
          ],
        ),
      )
    );
  }
}
