import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:flutter/material.dart';

class PageQuestionWrite extends StatefulWidget {
  const PageQuestionWrite({super.key});

  @override
  State<PageQuestionWrite> createState() => _PageQuestionWriteState();
}

/// Q & A 게시판 글 쓰는 기능을 페이지 입니다.
class _PageQuestionWriteState extends State<PageQuestionWrite> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body:
      SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(10),
          child: Center(
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child:Text('Q&A게시판 글쓰기'),
                ),

              Container(
               child: Column(
                  children: [
                    TextFormField(
                      decoration: const InputDecoration(
                        labelText: '제목',
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                        ),
                      ),
                    ),
                  Container(
                    padding: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          TextFormField(
                            maxLength: 1000,
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 5.0,
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                              ),
                            ),
                            maxLines: 11,
                          ),
                        ],
                       ),
                      ),
                    Container(
                      padding: EdgeInsets.all(15),
                        child: Column(
                          children: [
                            ElevatedButton(
                                 onPressed: () { // 글쓰기 완료 팝업창 뜨는
                                   showDialog(context: context, builder: ((context) => AlertDialog(
                                     title: Text(
                                         style: TextStyle(
                                           color: Colors.black87,
                                           fontSize: 22,

                                         ),'Q&a 게시판'
                                     ),
                                     content: Text('완료 되었습니다'),
                                     actions: [
                                       TextButton(
                                       onPressed: () => Navigator.pop(context, 'ok'), child: Text(
                                           style: TextStyle(
                                             color: Colors.black87,
                                             fontWeight: FontWeight.bold,
                                           ),
                                           'ok'),
                                    ),
                                     TextButton(onPressed: () => Navigator.pop(context, 'Cancel'), child: Text(
                                         style: TextStyle(
                                           color: Colors.black54,
                                           fontWeight: FontWeight.bold,
                                         ),
                                         'cancel'),
                                   ),
                                     ],
                                   )));
                                 },

                                  style: ElevatedButton.styleFrom(
                                //엘리베이터 버튼 스타일
                                  minimumSize: Size(200, 50), // 버튼 사이즈 조절
                                  backgroundColor: Colors.black,
                                  foregroundColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30)),
                                   ),
                                  child: Text('글쓰기'),
                            ),

                            Container(
                              padding:EdgeInsets.all(10),
                              child: ElevatedButton(
                                  onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                  //엘리베이터 버튼 스타일
                                  minimumSize: Size(200, 50), // 버튼 사이즈 조절
                                  backgroundColor: Colors.black,
                                  foregroundColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                 borderRadius: BorderRadius.circular(30)),
                                ),
                                      child: Text('취소'), ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
             ),
            ),
          ),
      ),
      );
  }
}
