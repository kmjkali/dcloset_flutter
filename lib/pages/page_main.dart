import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/components/component_goods_card.dart';
import 'package:dclost_app/model/goods/goods_response.dart';
import 'package:dclost_app/pages/page_goods_detail.dart';
import 'package:flutter/material.dart';

import '../model/goods/goods_item.dart';

class PageMain extends StatefulWidget {
  const PageMain({super.key});

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  List<GoodsItem> _list = [
    // GoodsResponse(1, 'assets/catalog1.png', '키치 코디 세트',  'assets/info1.png', 'assets/info2.png', 'assets/info3.png'),
    // GoodsResponse(2, 'assets/catalog2.png', '스포티 코디 세트',  'assets/info1.png', 'assets/info2.png', 'assets/info3.png'),
    // GoodsResponse(3, 'assets/catalog3.png', '페미닌 코디 세트',  'assets/info1.png', 'assets/info2.png', 'assets/info3.png'),
    // GoodsResponse(4, 'assets/catalog4.png', '트위드 코디 세트',  'assets/info1.png', 'assets/info2.png', 'assets/info3.png')
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text(
                '" 당신만의 놀라운 Daily Closet, 디클로젯 "',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
            Container(
              child: Image.asset(
                'assets/main.jpg',
                fit: BoxFit.cover,
                width: double.infinity,
                height: 470,
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text('DCloset Catalog',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
            Container(
              height: 200.0,
              child: GridView.builder(
                itemCount: _list.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    childAspectRatio: 3/4,
                  ), itemBuilder: (BuildContext ctx, int idx) {
                return ComponentGoodsCard(goodsItem: _list[idx], callback: () {
                  // @TODO 아래 내용 수정할 것 ( GoodsItem -> GoodsResponse 변환
                  //Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsDetail(goodsResponse: _list[idx])));
                }
                );
              }
              ),
            ),
          ],
        ),
      ),
    );
  }
}
