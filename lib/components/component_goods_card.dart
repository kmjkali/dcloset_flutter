import 'package:dclost_app/model/goods/goods_item.dart';
import 'package:flutter/material.dart';

// 제품 리스트 보여주기 폼

class ComponentGoodsCard extends StatelessWidget {
  const ComponentGoodsCard({
    super.key,
    required this.callback, required this.goodsItem
  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              height: 100,
              child: Image.asset(
                  goodsItem.productMainImage,
              fit: BoxFit.fitHeight,),
            ),
            Container(
              child: Text(goodsItem.productName,style: TextStyle(fontSize: 10),),
            ),
            Container(
              padding: EdgeInsets.all(5),
              child: Text(
                  goodsItem.productInfo,
              style: TextStyle(
                fontSize: 10
              ),),
            ),
          ],
        ),
      ),
    );
  }
}
